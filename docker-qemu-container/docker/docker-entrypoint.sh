#!/usr/bin/env bash

qemu-system-x86_64 \
  -name gnomeos \
  -m $QEMU_MEMORY \
  -smp $QEMU_CPUS \
  -bios /usr/share/qemu/OVMF.fd \
  -drive file=/gnomeos/disk.img,format=raw \
  -serial none \
  -parallel none \
  -nographic \
  -vga virtio \
  -vnc :0 \
  -device e1000,netdev=net0 \
  -netdev user,id=net0,hostfwd=tcp::2222-:22 \
  -device ich9-usb-ehci1,id=usb,bus=pci.0,addr=0x5.0x7 \
  -device ich9-usb-uhci1,masterbus=usb.0,firstport=0,bus=pci.0,multifunction=on,addr=0x5 \
  -device usb-tablet,id=input0,bus=usb.0,port=1
