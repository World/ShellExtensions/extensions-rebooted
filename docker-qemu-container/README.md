# GnomeOs on docker qemu
With these scripts you can run GnomeOs inside of a qemu VM inside of a docker container.

Note: This method is very slow since we are emulating and not virtualizing the environment.

## Prerequisites
On your host you will need these:
* docker
* qemu-img
* unxz
* parted

## Steps to run
1. Download the GnomeOs image without the installer. You can get it from here for now https://download.gnome.org/gnomeos/40.beta/gnome_os_no_installer_40.img.xz .
2. Execute `bash build-docker.sh` this will build the docker image to use. The image will be tagged `qemu-docker`.
3. Execute `bash prepare-img.sh` this will uncompress and resize the image, otherwise it would show a disk full error.
4. Execute `QEMU_MEMORY=4G QEMU_CPUS=4 bash run-docker.sh` this will start the docker container and start qemu with GnomeOs. Set the variables to what you need.
5. Connect with vnc. The container exposes the port 5900 to the host.
6. Wait some time, it can take several minutes for gnome to start up.
7. Go through the inital setup of gnome.
8. (optional) Setup sshd if you want to access via ssh. The container exposes the port 2222 to the host.
   1. Edit the file `/etc/sshd_config`, uncomment `PasswordAuthentication yes` and comment `UsePAM yes`.
   2. Execute `sudo systemctl enable sshd` and `sudo systemctl start sshd`.
   3. (optional) Add your public key to `~/.ssh/authorized_keys`.
9. (optional) Enable autologin of the user on the gnome settings.

If you shutdown the VM or stop the container, run only step 4 to start the VM again.

If you want to restart from scratch just delete the file `disk.img` and start from step 3 again.
