#!/usr/bin/env bash

extra_size='+2G'

unxz --verbose --keep disk.img.xz
qemu-img resize -f raw disk.img $extra_size
echo -e "print\nfix" | parted disk.img ---pretend-input-tty
parted disk.img resizepart 2 '100%'
parted disk.img print
