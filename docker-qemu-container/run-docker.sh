#!/usr/bin/env bash
QEMU_MEMORY=${QEMU_MEMORY:-4G}
QEMU_CPUS=${QEMU_CPUS:-4}

docker \
  run \
  -it \
  --rm \
  --name gnomeos \
  -e QEMU_MEMORY=$QEMU_MEMORY \
  -e QEMU_CPUS=$QEMU_CPUS \
  -p 5900:5900 \
  -p 2222:2222 \
  -v $PWD:/gnomeos \
  docker-qemu
