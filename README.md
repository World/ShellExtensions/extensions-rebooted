# Extensions Rebooted

## What is Extensions Rebooted?

Extensions Rebooted is an attempt to address the myriad of issues around the GNOME Shell extension ecosystem. Primarily we want to build a process around how extensions are accepted into extensions website and how they are supported.

This project contains a number of interesting elements:

* Devops - building a container that will run gnome-shell on a virtual framebuffer X11 server that will be deployed in a CI pipeline as part of an automated process that will be determine if an extension is working.
* Bring extensions as part of the GNOME release process and provide early warning to extension writers that their extension does not work on the latest release.
* Centralization of gnome-extensions to the GNOME Gitlab so that all extensions can be seen and tested. Developers are free to develop their extension on any platform, but must push to the GNOME gitlab service for submission.
* A set of policies and requirements that will hold extension writers accountable for maintaining their extensions including unit tests.
* Use static analysis of extensions to determine safety
* Building a self sustaining community around extensions that involves extension writers and gnome shell developers working together.

If you are interested in helping, several issues will have "[help wanted](https://gitlab.gnome.org/World/ShellExtensions/extensions-rebooted/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Help%20Wanted)" labels that you can jump in.

Skillsets needed:
* infrastructure/build engineers w/devops, CI/CD experience
* javascript coding experience
* technical writing experience 
* javascript/gobject experience
* community management experience
* editors
* graphics artist / frontend web skills

All skillsets are welcome.

Questions? Feedback? Join us on our Matrix [Channel](https://gnome.element.io/#/room/#extensions:gnome.org)
