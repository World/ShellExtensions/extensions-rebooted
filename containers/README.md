# Headless GNOME Shell testing

The scripts written here will be used to create an infrastructure that will test extensions in a headless configuration of GNOME Shell.

How to build the CI container:

 * use the build-ci-container to build the container - please be aware that this uses buildah to build the container so you'll need to use
   a system that has buildah and podman available.

 * Download a zip file of an extension - preferably a very simple one - https://extensions.gnome.org/extension/83/lock-screen/

 * container=$(podman ps -l | tail -1 | cut -d" " -f1) 
   podman cp extension.zip $container:/home/gnomeshell

 * run podman --cgroup-manager=cgroupfs run -ti -p 1818:22 --security-opt label=disable --security-opt seccomp=unconfined --cap-add all 44e241c7252e /sbin/init in a terminal

 * then ssh into the container with ssh gnomeshell@localhost -p 1818 
   the password is 'gnomeshell'

 * run test-extensions.sh extension.zip
