# Copyright (C) 2020 GNOME Foundation
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Author: Sriram Ramkrishna <sri@gnome.org>
#
# The original script is a snippet written by Florian Müllner

#!/bin/bash

function cleanup()
{
	echo cleaning up...
	rm -rf $testdir
}

zip=$1


[ -f $zip ] || exit 1

testdir=`mktemp -p $XDG_RUNTIME_DIR -d extension-test.XXXXXX` || exit 1

echo $testdir

export XDG_CACHE_HOME=$testdir/cache
export XDG_CONFIG_HOME=$testdir/config
export XDG_DATA_HOME=$testdir/data

gnome-extensions install $zip

export GSETTINGS_BACKEND=keyfile

uuid=$(basename $XDG_DATA_HOME/gnome-shell/extensions/*)
gsettings set org.gnome.shell enabled-extensions "['$uuid']"

extension=$(gsettings get org.gnome.shell enabled-extensions)

# HACK: not running gnome-session, so the D-Bus activation won't be updated;
#   guess the display that mutter will export
#export WAYLAND_DISPLAY=wayland-1

trap cleanup SIGINT

XDG_SESSION_TYPE=x11 xvfb-run -s '+iglx -noreset' gnome-shell | systemd-cat -t gnomeshell &
pid="$!"

sleep 35
gsettings set org.gnome.shell disabled-extensions $extension


kill -TERM $pid
