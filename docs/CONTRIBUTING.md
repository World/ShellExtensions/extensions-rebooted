# Contribution Rules

- Respect 80 characters margin for JavaScript examples. XML blocks can use more characters since breaking them can cause less readablity.

- Always specify the code block type. For example starting xml code block with *\```* is bad. You should use *\```xml*.

- The indentation is 4 characters. Don't use tab for indentation.

- Avoid unnecessary comments. The code should be self explanatory.

- Write a simple and readable code.

- Follow the [GNOME Shell code style](https://gitlab.gnome.org/GNOME/gnome-shell/-/blob/master/HACKING.md).

- If your example need some explaining, do it under the example block. If you need to explain more than one topic form that code block, keep each topic in a list item.

- Don't use bold texts unless you really have to.

- Use inline code block for namespaces, adressing classes, functions and methods (like `ui.main`, `ui.windowPreview.WindowPreview`), use italic otherwise (like *GtkToggleButton*, *buildPrefsWidget()*).

- Test your examples before posting it.

*This file may be updated in the future. Any change should be applied to the documents.*
