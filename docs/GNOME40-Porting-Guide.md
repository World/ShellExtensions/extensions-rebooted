# Port Extensions to GNOME Shell 40

The port documentation is now part of [gjs.guide](https://gjs.guide/) and you can read it in [Port Extensions to GNOME Shell 40](https://gjs.guide/extensions/upgrading/gnome-shell-40.html) page.
